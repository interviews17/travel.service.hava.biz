import { FastifyInstance }  from 'fastify';
import recordRoutes from './record';

export default function(app: FastifyInstance){


    app.register(recordRoutes, { prefix: '/api/v1/record'} );


}