import {FastifyInstance} from 'fastify'
import Requests from "../../../utils/requests"
import requestmaker from '../../../utils/reqmaker'
import { RecordController } from '../../../controllers/record_controller'
import Responses from '../../../utils/responses'

export default function (app: FastifyInstance, options, done){

    const countryProp = {
        id: { type: 'number' },
        status: { type: 'string' },
        request_date: { type: 'string' },
        pickup_lat: { type: 'number' },
        pickup_lng: { type: 'number' },
        pickup_location: { type: 'string' },
        dropoff_lat: { type: 'number' },
        dropoff_lng: { type: 'number' },
        dropoff_location: { type: 'string' },
        pickup_date: { type: 'string' },
        dropoff_date: { type: 'string' },
        type: { type: 'string' },
        driver_id: { type: 'number' },
        driver_name: { type: 'string' },
        driver_rating: { type: 'number' },
        driver_pic: { type: 'string' },
        car_make: { type: 'string' },
        car_model: { type: 'string' },
        car_number: { type: 'string' },
        car_year: { type: 'number' },
        car_pic: { type: 'string' },
        duration: { type: 'number' },
        duration_unit: { type: 'string' },
        distance: { type: 'number' },
        distance_unit: { type: 'string' },
        cost: { type: 'number' },
        cost_unit: { type: 'string' },
    }
    
    // FETCH ALL RECORDS
    const fetchAllSchema = {
        tags: ['records'], summary: 'Fetch all records',
        response: {
            400: Responses.errorResponseObj(),
            200: Responses.successResponseObj({
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        ...countryProp 
                    },
                    additionalProperties: true
                }
            })
        }
    }
    app.get('/fetch-all', { schema: fetchAllSchema }, RecordController.fetch );





    // FETCH ALL FILTERED RECORDS
    const fetchFIlteredSchema = {
        tags: ['records'], summary: 'Fetch filtered records',
        body: Requests.requestBody({
            Keyword: { type: 'string', description: 'Keyword' },
            status: { type: 'string', description: 'status cancelled' },
            min_distance: { type: 'number', description: 'distance' },
            max_distance: { type: 'number', description: 'distance' },
            min_duration: { type: 'number', description: 'duration' },           
            max_duration: { type: 'number', description: 'duration' },           
        }, []),
    }
    app.post('/fetch-filtered', { schema: fetchFIlteredSchema }, RecordController.fetchByFilter);

    // FETCH ALL FILTERED RECORD DETAILS
    const fetchDetailsSchema = {
        tags: ['records'], summary: 'Fetch filtered records',
        params: Requests.requestParams({
            id: { type: 'number', description: 'filtered record detail' }
        }),
    }
    app.get('/fetch-details/:id', { schema: fetchDetailsSchema }, RecordController.details);    



    done();
}