import { FastifyInstance }  from 'fastify';
import Responses from '../utils/responses';

import havaRoutes from './v1/hava'


export default function(app: FastifyInstance) {


    havaRoutes(app);


    app.setNotFoundHandler((req, res) => {
        res.code(404).send(Responses.errorResponses('001', 'Page not found', []));
    })

    app.setErrorHandler((error, req, res) => {
        res.code(500).send(Responses.errorResponses('001', 'Server error', [{ message: error.message }]));
    })

}