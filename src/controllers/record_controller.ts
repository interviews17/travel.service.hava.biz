import {FastifyRequest, FastifyReply} from 'fastify';
import { reqValidator } from "../utils/validator"
import Responses from "../utils/responses"
import requestmaker from '../utils/reqmaker'


export class RecordController{

    static async fetch(req: FastifyRequest, res: FastifyReply) {
        console.log("HIT");
        
        try {

            let record;
            try {
                record = await requestmaker({
                    service: 'hava',
                    action: 'trips/recent.json',
                    method: 'GET'
                })
            } catch (ex) {
                return res.code(400).send(ex)
            }

            res.send(Responses.successResponses('000', 'Travel record', record["trips"]))
            
        } catch (error) {
            res.code(400).send(Responses.errorResponses('001', 'Invalid request', []));
        }
    };

    static async fetchByFilter(req: FastifyRequest, res: FastifyReply) {
        const parameters: any = req.body;


        
        
        
        try {
            let record;
            let filtered = [];
            record = await requestmaker({
                service: 'hava',
                action: 'trips/recent.json',
                method: 'GET'
            })
            let filteredData = [...record["trips"]]
            try {
                
                const dataKeys = Object.keys(parameters);
                const currentData = {};
                for (let item of dataKeys) {
                  if (parameters[item]) {
                    currentData[item] = parameters[item];
                  }
                }
                for (let el in currentData) {
                    console.log(el, currentData[el]);

                    if(el === "min_distance") {
                        filteredData = filteredData.filter(
                            (element) => element["distance"] >= currentData[el]
                        );
                        // console.log(filteredData);
                        
                    }
                    else if(el === "max_distance") {
                        filteredData = filteredData.filter(
                            (element) => element["distance"] <= currentData[el]
                        );
                    }
                    else if(el === "min_duration") {
                        filteredData = filteredData.filter(
                            (element) => element["duration"] >= currentData[el]
                        );
                        
                    }
                    else if(el === "max_duration") {
                        filteredData = filteredData.filter(
                            (element) => element["duration"] <= currentData[el]
                        );
                        
                    }else if(el === "status"){
                        filteredData = record["trips"].filter(
                            (element) => element["status"] === currentData[el]
                        );
                    }else if(el === "Keyword"){
                        console.log( currentData[el]);
                
                        function filter(array, value, key) {
                            return array.filter(key
                                ? a => a[key] === value.toLowerCase()
                                : a => Object.keys(a).some(k => a[k] === value)
                            );
                        }
                        let re = filter(filteredData, currentData[el], '');
                        filteredData = re;

                    }
                }

                
            } catch (ex) {
                return res.code(400).send(ex)
            }

            res.send(Responses.successResponses('000', 'Travel record', filteredData))
            
            
        } catch (error) {
            res.code(400).send(Responses.errorResponses('001', 'Invalid request', []));
        
        }

    };

    static async details(req: FastifyRequest, res: FastifyReply) {

        try {
            let record;
            let result;
            try {
                record = await requestmaker({
                    service: 'hava',
                    action: 'trips/recent.json',
                    method: 'GET'
                })

                result = record["trips"].filter((el) => parseInt(el.id) == req.params["id"] );
            } catch (ex) {
                return res.code(400).send(ex)
            }

            res.send(Responses.successResponses('000', 'Travel record', result))
            
            
        } catch (error) {
            res.code(400).send(Responses.errorResponses('001', 'Invalid request', []));
        
        }

    };    


}