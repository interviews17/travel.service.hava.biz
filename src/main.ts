import path from 'path';

import Fastify from 'fastify';
import helmet from 'fastify-helmet';
import fastifyStatic from 'fastify-static';
import fastifyFormbody from 'fastify-formbody';
import fastswagger from 'fastify-swagger';
import cors from 'fastify-cors';
import {env} from './utils/env'
import routes from './routes';

// GET ENVIRONMENT VARIABLES
const envVars = env();

// SERVER
const app = Fastify({
    logger: true,
    pluginTimeout: 100000,
    trustProxy: true
});

// PLUGINS
app.register(helmet, { contentSecurityPolicy: false });
app.register(cors);
app.register(fastifyFormbody);
app.register(fastifyStatic, {
    root: path.join(__dirname, '/../public'),
});
app.register(fastswagger, {
    exposeRoute: envVars.fastify ? true : false,
    routePrefix: '/api/documentation',
    swagger: {
        info: {
            title: 'Hava backend service',
            description: 'A service to retrive and flter travel logs',
            version: envVars.version
        },
        host: envVars.host+":"+envVars.port,
        schemes: [envVars.scheme],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: [],
        securityDefinitions: {
            "Authorization": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header"
            },
            "client_id": {
                "type": "apiKey",
                "name": "client_id",
                "in": "header"
            }
        }
    }
});

// ROUTES
routes(app);

app.listen(envVars.port, '0.0.0.0', function (err, address) {
    if (err) {
        app.log.error(err.message);
        process.exit(1);
    }
    app.log.info(`Server listening on port ${address}`);
});

export default app; 
