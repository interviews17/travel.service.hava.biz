# requests [ Node js - fastify framework ]

This repo is a  nodejs production ready set up.

---

## Requirements and installation
* git clone https://gitlab.com/interviews17/travel.service.hava.biz.git
* cd node-template
* mkdir .env and add a port, node_env and a project name
    * API_PORT=8030
    * API_HOST=localhost
    * PROJECT_NAME=travel.service
    * API_VERSION=0.0.0.1
    * API_SCHEME=http
    * NODE_ENV=development
    * FASTIFY=1
    * DOCKER_ENV=


 
Install globally
* npm install -g ts-node
* npm install -g typescript
* npm install -g sequelize-cli-typescript 
* npm install -g nodemon 

Run on locally in project folder
* npm install

Run service
* nodemon

Build docker container
* Open docker file
* right click in it and select build image and hit enter


![myimage-alt-tag](reqs.png)
